
/********************************************* FORTYWINKS ************************************************/
/*********************************************************************************************************/



/********************************************* FORTYWINKS ************************************************/
/*********************************************************************************************************/
jQuery('.os-step .step-title h2').click(function(){
    var boxParent = jQuery(this).parents('.os-step');
    boxParent.toggleClass('showed');   //
    boxParent.find('.box-step').stop().slideToggle();
    boxParent.siblings().find('.box-step').slideUp();
    boxParent.siblings().removeClass('showed');
});



/********************************************* VIOLET BOX ************************************************/
/*********************************************************************************************************/
// @ENHANCE: KEY ONCLICK
triggerHomekey = function(){
    jQuery(document).keydown(function(e) {
        slideHomeDesktop = jQuery('#owl-sidershow-home-page');
        if ( e.keyCode == 37 )
            slideHomeDesktop.trigger('owl.prev');
        else if ( e.keyCode == 39 )
            slideHomeDesktop.trigger('owl.next');
    });
};
/* @FIX: fix input pop at ipad */
var notStickyinputs =  jQuery('input');
notStickyinputs.on('focus', function(){
    jQuery('.sticky-bar').addClass('fixIosInput');
});
notStickyinputs.on('blur', function(){
    jQuery('.sticky-bar').removeClass('fixIosInput');
});
/* @CUSTOMERSERVICE-PAGE @FAQS-PAGE: fix scroll when you click hreft# */
if ( (jQuery(".cms-customer-service").length > 0 ) || (jQuery(".cms-faqs-html").length > 0) ){
    jQuery(function(){
        jQuery('.disc a').click( function(e) {
            e.preventDefault();
            var destination, offset;
            offset = jQuery(jQuery(this).attr('href')).offset().top;
            if( isMobileScreen() ){        //mobile
                destination = offset - 2;
            } else {                                                        // ipad & desktop
                var sticky = jQuery(".sticky-bar").height();
                destination = offset - sticky - 5;
            }
            jQuery('body, html').animate({
                scrollTop   :   destination
            }, 320);
        });
    });
}

